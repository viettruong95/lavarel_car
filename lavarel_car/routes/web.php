<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	
Route::get('/',  'ShopController@home');
Route::get('/home', 'ShopController@home');
Route::group(['middleware' => ['web']],function(){
	Route::get('/admin/login','AdminController@login');
	Route::post('/admin/handle_login','AdminController@handle_login');
	Route::get('/admin/handle_logout','AdminController@handle_logout');
	Route::get('/admin/home', 'AdminController@home');
	Route::get('/admin/', 'AdminController@home');
	Route::get('/admin/car_manager', 'AdminController@car_manager');
	Route::get('/admin/add_car', 'AdminController@add_car');
	Route::post('/admin/handle_add_car', 'AdminController@handle_add_car');
	Route::get('/admin/edit_car/{id_car}', 'AdminController@edit_car');
	Route::post('/admin/handle_edit_car', 'AdminController@handle_edit_car');
	Route::get('/admin/handle_delete_car/{id_car}', 'AdminController@handle_delete_car');

});

