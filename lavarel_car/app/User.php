<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $table = 'users';
    protected $fillable = ['id', 'user_name'];
    protected $hidden = ['password'];
    public $timestamps = false;
}
