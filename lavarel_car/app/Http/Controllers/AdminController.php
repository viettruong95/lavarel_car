<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\User;
use App\Car;
class AdminController extends Controller
{

	public function home()
	{
		if (session::has('user_name')){
			return view('admin.home');
		}else{
			return  redirect('admin/login');
			
		}

	}

	public function login()
	{

		return view('admin.login');
	}  
	public function handle_logout()
	{
		session::flush();
		return  redirect('admin/login');
	}  

	public function handle_login(Request $request)
	{
		$user = User::where('user_name', $request->user_name)
		->where('password',md5($request->password))
		->first();

		if($user){
			session::put('user_name', $user->user_name);
			return  redirect('admin/home');
		}else{
			return  redirect()->back();

		}
	}
	public function car_manager()
	{
		if (session::has('user_name')){
				$listcar = Car::paginate(8);
			return view('admin.car_manager', ['listcar' => $listcar]);
		}else{
			return  redirect('admin/login');
			
		}

	}
	public function add_car()
	{
		if (session::has('user_name')){
			return view('admin.add_car');
		}else{
			return  redirect('admin/login');
			
		}

	}
	public function handle_add_car(Request $request)
	{
		$newcar = new Car;
		$newcar->name = $request->name;
		$newcar->description = $request->description;
		$newcar->price = $request->price;
		$newcar->image = $request->default_image;
		if($request->hasFile('image')){
			$file = $request->image;
			$newcar->image = $file->move('public/img',$file->getClientOriginalName());

		}
		$newcar->save();
		return  redirect('admin/car_manager');
	}
	public function edit_car($id_car)
	{
		$car = Car::find($id_car);
		if (session::has('user_name')){
			return view('admin.edit_car', ['car' => $car]);
		}else{
			return  redirect('admin/login');
			
		}

	}
	public function handle_edit_car(Request $request)
	{
		$car = Car::find($request->id);
		$car->name = $request->name;
		$car->description = $request->description;
		$car->price = $request->price;
		$car->image = $request->old_image;
		if($request->hasFile('image')){
			$file = $request->image;
			$car->image = $file->move('public/img',$file->getClientOriginalName());
			
		}
		$car->save();
		return  redirect('admin/car_manager');
	}

	public function handle_delete_car($id_car)
	{
		$car = Car::find($id_car)->delete();
		return  redirect('admin/car_manager');
	}
}
