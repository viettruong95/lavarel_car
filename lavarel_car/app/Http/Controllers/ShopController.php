<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
class ShopController extends Controller
{
   
    public function home()
    {
    	$listcar = Car::paginate(6);
        return view('shop.home', ['listcar' => $listcar]);
    }

    
}
