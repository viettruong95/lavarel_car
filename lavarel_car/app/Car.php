<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
	protected $table = 'cars';
	protected $fillable = ['id', 'name','image','description','price'];
	public $timestamps = false;
}
