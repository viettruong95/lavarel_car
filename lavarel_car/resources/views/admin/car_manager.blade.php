@extends('layout_admin')
@section('title', 'Quản lý xe')

@section('content')
<h2>Quản lý xe</h2>
<hr>
<a href="{{url('/admin/add_car')}}"><button class="btn btn-primary" style="float: right; margin-bottom: 20px" >Thêm mới xe</button></a>
<table class="table table-bordered table-striped" style="text-align: center;">
	<thead >
		<tr >
			<th style="text-align: center;">Ai đi</th>
			<th style="text-align: center;">Ảnh xe</th>
			<th style="text-align: center;">Tên xe</th>
			<th style="text-align: center;">Mô tả xe</th>
			<th style="text-align: center;">Giá xe</th>
			<th></th>
			<th></th>

		</tr>
	</thead>
	<tbody>
		@foreach ($listcar as $car )
		<tr>
			<td>{{$car->id}}</td>
			<td><img src="../{{$car->image}}" style="width: 100px; height: 100px"></td>
			<td>{{$car->name}}</td>
			<td>{{$car->description}}</td>
			<td>{{$car->price}}</td>
			<td><a href="{{url('admin/edit_car',$car->id)}}">Sửa</a></td>
			<td><a href="{{url('admin/handle_delete_car',$car->id)}}" onclick="return confirm('Bạn có chắc muốn xóa sản phẩm này ko')">Xóa</a></td>
		</tr>
		
		@endforeach

	</tbody>
</table>
	{!! $listcar->links() !!}
@endsection