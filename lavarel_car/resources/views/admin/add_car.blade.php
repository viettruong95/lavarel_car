@extends('layout_admin')
@section('title', 'Thêm mới xe')

@section('content')
<h2>Thêm mới xe</h2>
<hr>
<form class="form-horizontal" action="{{ url('/admin/handle_add_car') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field()}}
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Tên xe:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="name" placeholder="Nhập tên của xe" >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Mô tả xe:</label>
		<div class="col-sm-4">
			<textarea type="text" class="form-control" name="description" placeholder="Nhập mô tả về xe" rows="5" ></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Giá xe:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="price" placeholder="Nhập giá của xe" >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Ảnh xe:</label>
		<div class="col-sm-4">
			<input type="file" class="form-control" id="chooseimg" name="image" placeholder="Nhập tên xe" >
			<input type="hidden" name="default_image" value="public/img/default_images.png">
			<img id="image" height="350px" width="330px" style="margin-top: 20px" src="../public/img/default_images.png" />
		</div>

	</div>
	<input class="btn btn-primary col-sm-offset-4" type="submit" name="submit_add_car" value="Thêm mới">
</form>
<script type="text/javascript">
	var file = document.getElementById('chooseimg');
	var img = document.getElementById('image');
	file.addEventListener("change", function() {
		if (this.value) {
			var file = this.files[0];
			var reader = new FileReader();
			reader.onloadend = function () {
				img.src = reader.result;
				
			};
			reader.readAsDataURL(file);
		}
	});
</script>
@endsection
