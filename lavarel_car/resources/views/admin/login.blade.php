<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

	<style type="text/css">

	.container-login
	{
		margin-top: 35%;
		animation-name: example;
		animation-duration: 1s;
	}
	@-webkit-keyframes example {
		from {margin-top: -35%;}
		to {margin-top: 35%;}
		
	}
	#panel-login{
		background-color: #222;
		border: black;
	}


	#title-login
	{
		text-align: center;
		color: white;
	}


</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="container-fluid container-login">
					<div class="panel panel-default" id="panel-login">
						<div class="panel-body">
							<h2 id="title-login">Đăng nhập</h2>    
							<hr>
							<form  action="{{ url('/admin/handle_login') }}" method="POST" role="form">
								{{ csrf_field()}}
								<div class="form-group">
									<strong><label style="color:white">Tên đăng nhập:</label></strong>
									<input type="text" class="form-control" id="user_name" name="user_name" >
								</div>
								<div class="form-group">
									<strong><label style="color:white">Mật khẩu:</label></strong>
									<input type="password" class="form-control" id="password" name="password">
								</div>
								<button type="submit" class="btn btn-primary">Đăng nhập</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
		</div>
	</div>
</body>
</html>