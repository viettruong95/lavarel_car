@extends('layout_admin')
@section('title', 'Sửa thông tin xe')

@section('content')
<h2>Thêm mới xe</h2>
<hr>
<form class="form-horizontal" action="{{ url('/admin/handle_edit_car') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field()}}
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Tên xe:</label>
		<div class="col-sm-4">
			<input type="hidden" class="form-control" name="id"value="{{$car->id}}" >
			<input type="text" class="form-control" name="name" placeholder="Nhập tên của xe" value="{{$car->name}}" >
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Mô tả xe:</label>
		<div class="col-sm-4">
			<textarea type="text" class="form-control" name="description" placeholder="Nhập mô tả về xe" rows="5" >{{$car->description}}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Giá xe:</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="price" placeholder="Nhập giá của xe" value="{{$car->price}}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" >Ảnh xe:</label>
		<div class="col-sm-4">
			<input type="file" class="form-control" id="chooseimg" name="image" placeholder="Nhập tên xe">
			<input type="hidden" name="old_image"  value="{{$car->image}}">
			<img id="image" height="350px" width="330px" style="margin-top: 20px" src="../../{{$car->image}}" />
		</div>

	</div>
	<input class="btn btn-primary col-sm-offset-4" type="submit" name="submit_add_car" value="Sửa xe">
</form>
<script type="text/javascript">
	var file = document.getElementById('chooseimg');
	var img = document.getElementById('image');
	file.addEventListener("change", function() {
		if (this.value) {
			var file = this.files[0];
			var reader = new FileReader();
			reader.onloadend = function () {
				img.src = reader.result;
			};
			reader.readAsDataURL(file);
		}
	});
</script>
@endsection
