@extends('layout')
@section('title', 'Trang Chủ')

@section('content')


	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
			</div>

			<div class="item">
				<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
			</div>

			<div class="item">
				<img src="http://penang.wowrentalcar.com/wp-content/uploads/2017/02/wowrentalcar_slide1_3.jpg" style="width:100%;">
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

<div class="container">



    <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Siêu xe
                    <small>Mới nhất</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

    <div class="container">
         <!-- Projects Row -->
        <div class="row">
        	@foreach($listcar as $car)
        
            <div class="col-md-4 portfolio-item" style="height: 530px;  padding: 15px">
            	<div class="listcar" style="height: 530px; border: 1px solid black; padding: 5px">
            		 <a href="#">
                    <img  src=" {{$car->image}}"  height="350px" width="100%">
                </a>
                <h3>
                    <a href="#">{{ $car->name}}</a>
                </h3>
                <p style="height: 58px; overflow: hidden;">{{ $car->description}}</p>
                <p><h3><strong>Giá: {{ $car->price}} VNĐ</strong></h3></p>
            	</div>
               
            </div>
            	@endforeach
            	{!! $listcar->links() !!}
        </div>
        <!-- /.row -->
    </div>
    
    
    
     <!-- Page Content -->
    <div class="container">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Text on Right
                    <small>Secondary Text</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Project One -->
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Project One</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium veniam exercitationem expedita laborum at voluptate. Labore, voluptates totam at aut nemo deserunt rem magni pariatur quos perspiciatis atque eveniet unde.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Two -->
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Project Two</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Three -->
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Project Three</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, temporibus, dolores, at, praesentium ut unde repudiandae voluptatum sit ab debitis suscipit fugiat natus velit excepturi amet commodi deleniti alias possimus!</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>
    </div>

    
</div>
@endsection