<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

	<link rel="stylesheet"  href="{{asset('public/css/style_admin.css')}}">
</head>
<body>
	<header class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="#">Car-Shop</a>
					</div>
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#">Page 1</a></li>
						<li><a href="#">Page 2</a></li>
					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search">
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav" style="float: right;">
					
						<li><a href="#">Xin chào {{ session::get('user_name')}}</a></li>
						<li><a href="{{url('/admin/handle_logout')}}">Đăng xuất</a></li>
					</ul>
					
				</div> 
			</nav>
		</div>
		
	</header>


	<div class="container-fluid">
		<div class="row">
			<div class="left-menu col-md-3">
				<div class="vertical-menu">
				
					<a href="">	<center><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOyNEdJYAO5Uilr8-ko3fF02yrzXsyVnLhDurj0Cu4nEDjWcW14w"  width="150" height="150" style=" border-radius: 50%;"><h3>Name Shop</h3></center></a>
					<a href="{{url('/admin/car_manager')}}" class="menu"><h4 ><span class="fa fa-car"></span> Quản lý xe</h4></a>
					<a href="#" class="menu"><h4 ><span class="fa fa-music"></span> Quản lý phụ kiện</h4></a>
					<a href="#" class="menu"><h4 ><span class="fa fa-user"></span> Quản lý nhân viên</h4></a>
					<a href="#" class="menu"><h4 ><span class="fa fa-book"></span> Quản lý đơn hàng</h4></a>
					<a href="#" class="menu"><h4 ><span class="fa fa-image"></span> Quản lý slide</h4></a>
				</div>
			</div>

			<!-- Main Content -->
			<div class="container-fluid">
				<div class="side-body col-md-9">
					@yield('content')

				</div>
			</div>
		</div>

	</div>



	<footer style="background-color: black; margin-top:20px;" >
		<div class="container-fluid text-center">
			<div class="row">
				<div id="footer">
					<div class="col-xs-12 col-sm-3 col-md-3">
						<h4>Quick links</h4>
						<ul class="list-unstyled quick-links">
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Get Started</a></li>
							<li><a href="#"></i>Videos</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3">
						<h4>Quick links</h4>
						<ul class="list-unstyled quick-links">
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Get Started</a></li>
							<li><a href="#"></i>Videos</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3">
						<h4>Quick links</h4>
						<ul class="list-unstyled quick-links">
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Get Started</a></li>
							<li><a href="#"></i>Videos</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3">
						<h4>Quick links</h4>
						<ul class="list-unstyled quick-links">
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Get Started</a></li>
							<li><a href="#"></i>Videos</a></li>
						</ul>
					</div>
				</div>	
				<div class="text-center center-block">
					<br />
					<a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
					<a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
					<a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
					<a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
				</div>
				<HR/>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
						<p><u><a href="https://www.nationaltransaction.com/">National Transaction Corporation</a></u> is a Registered MSP/ISO of Elavon, Inc. Georgia [a wholly owned subsidiary of U.S. Bancorp, Minneapolis, MN]</p>
						<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="https://www.sunlimetech.com" target="_blank">Sunlimetech</a></p>
					</div>
				</hr>
			</div>	
		</div>
	</footer>
	<script type="text/javascript">
		$(function () {
			$('.navbar-toggle').click(function () {
				$('.navbar-nav').toggleClass('slide-in');
				$('.side-body').toggleClass('body-slide-in');
				$('#search').removeClass('in').addClass('collapse').slideUp(200);

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');
        
    });

   // Remove menu for searching
   $('#search-trigger').click(function () {
   	$('.navbar-nav').removeClass('slide-in');
   	$('.side-body').removeClass('body-slide-in');

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').removeClass('slide-in');

    });
});
</script>
</body>
</html>